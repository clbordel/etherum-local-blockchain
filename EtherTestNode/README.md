# Resource/Package Info

Geth - https://geth.ethereum.org/downloads/

# How-Tos

Basic guide here ->

https://mobycrypt.medium.com/setup-ethereum-development-environment-in-5-minutes-51336c013fdb

Necessary to updated `genesis.json` with new eip block protocols --> 

https://github.com/ethereum/go-ethereum/issues/20676

# Quick start

1. Create a test wallet

./geth/geth-linux-amd64-1.10.4-aa637fd3/geth --datadir ./EtherTestNode account new

2. Initialize the blockchain

geth/geth-linux-amd64-1.10.4-aa637fd3/geth --datadir ./EtherTestNode init ./EtherTestNode/genesis.json

3. Run the node on port 6666 and enable API acces

./geth/geth-linux-amd64-1.10.4-aa637fd3/geth --rpc --rpcaddr localhost --rpcport 6666 --rpcapi "personal,eth,web3,net" --datadir ./EtherTestNode console


The node can be curled using the following command to check the wallet balance:

curl --data-binary '{"jsonrpc":"2.0","id":"curltext","method":"eth_getBalance","params":["0xF0b54A473416a2166cE0d0eB0ba8d15F6c88D979","latest"]}' -H 'content-type:application/json;' http://localhost:6666 -vvvv
